﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StoreProject.Models;

namespace StoreProject.Controllers
{
    public class PartController : Controller
    {
        private StoreContext db = new StoreContext();

        // GET: /Part/
        public ActionResult Index()

        {

            return View(db.Part.ToList());
        }

        // GET: /Part/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Part part = db.Part.Find(id);
            
            if (part == null)
            {
                return HttpNotFound();
            }
            return View(part);
        }

        // GET: /Part/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Part/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PartId,name,Description,packed,picture")] Part part, HttpPostedFileBase ImageFile)
        {
            if (ModelState.IsValid)
            {
                //no part yet 
                if (ImageFile != null)
                {
                    part.picture= System.IO.Path.GetExtension(ImageFile.FileName);
                   
                }
                db.Part.Add(part);
                db.SaveChanges();

                // part exist
                if (ImageFile != null)
                {
                    string picName = part.PartId + System.IO.Path.GetExtension(ImageFile.FileName);
                    string path = System.IO.Path.Combine(Server.MapPath("~/images/part"), picName);
                    ImageFile.SaveAs(path);
                }

                long id = part.PartId;
                return RedirectToAction("addPiece", new { id });
                
            }

            return View(part);
        }

        // Add Piece to a part 
        public ActionResult addPiece(long? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Part part = db.Part.Find(id);
            if (part == null)
            {
                return HttpNotFound();
            }

            int count;
            if (part.packed)
            {

                 count = (from p in db.Piece where p.PartId == id  select (int?) p.quantity).Sum()?? 0;
                

            }
            else
            {

               count = (int?)part.pieces.Count()?? 0;
            }

           



            ViewData["Stock"] = count;
            ViewData["SelectedPart"] = part;

            return View();
        }

        //Create the Piece
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePiece([Bind(Include = "PartId,location,owner,status,quantity")] Piece piece)
        {
            if (ModelState.IsValid)
            {
               

                db.Piece.Add(piece);
                db.SaveChanges();
                long id = (long)piece.Id;

                return RedirectToAction("QrGenerate", new { id });
            }

            return RedirectToAction("Index");
        }

        // Generating the Qr
        public ActionResult QrGenerate(long id)
        {

            Piece piece = db.Piece.Find(id);
            return View(piece);
        }

        // GET: /Part/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Part part = db.Part.Find(id);
            if (part == null)
            {
                return HttpNotFound();
            }
            return View(part);
        }

        // POST: /Part/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PartId,name,Description,packed,picture")] Part part, HttpPostedFileBase ImageFile, String Remove)
        {
            if (ModelState.IsValid)
            {
                if (string.Equals(Remove, "Remove"))
                {
                    try
                    {
                        string path = "~/images/part/" + part.PartId + part.picture;
                        System.IO.File.Delete(path);
                    }
                    catch (Exception) { }
                    part.picture = null;
                    db.Entry(part).State = EntityState.Modified;
                }
                else
                {
                    if (ImageFile != null)
                    {
                        //
                        part.picture = System.IO.Path.GetExtension(ImageFile.FileName);
                        string picName = part.PartId + part.picture;
                        string path = System.IO.Path.Combine(Server.MapPath("~/images/part"), picName);
                        ImageFile.SaveAs(path);
                        db.Entry(part).State = EntityState.Modified;
                    }
                    else
                    {
                        Part partTemp = db.Part.Find(part.PartId);
                        partTemp.name = part.name;
                        partTemp.Description = part.Description;


                        db.Entry(partTemp).State = EntityState.Modified;

                    }
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(part);
        }

        // GET: /Part/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Part part = db.Part.Find(id);
            if (part == null)
            {
                return HttpNotFound();
            }
            return View(part);
        }

        // POST: /Part/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Part part = db.Part.Find(id);
            db.Part.Remove(part);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
