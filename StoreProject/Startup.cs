﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(StoreProject.Startup))]
namespace StoreProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
