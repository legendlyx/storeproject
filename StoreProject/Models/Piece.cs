﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StoreProject.Models
{
    public class Piece
    {
        public long Id { get; set; }
        public long PartId { get; set; }
        [Required]
        public String owner { get; set; }
        [Required]
        public String location { get; set; }
        public int quantity { get; set; }
        public String status { get; set; }

        public Piece ()
        {

            this.quantity = -1;
        }

        public virtual Part part { get; set; }
    }
}