﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace StoreProject.Models
{
    public class StoreContext: DbContext 
    {
        public DbSet<Part> Part { get; set; }
        public DbSet<Piece> Piece { get; set; }
     }
}