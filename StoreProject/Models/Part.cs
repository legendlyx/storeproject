﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StoreProject.Models
{

    // part table 
    public class Part
    {


        // Fields 
        public long PartId { get; set; }
        [Required]
        public String name { get; set; }
        [Required]
        public String Description { get; set; }
        public bool packed { get; set; } 
        public String picture { get; set; }

        //Foreign Key  

       public virtual ICollection<Piece> pieces { get; set; }




     
    }
}